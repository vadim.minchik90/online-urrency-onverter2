package test;

public class BasicInformation {
	private String bankName;
	private String buyingDollar;
	private String dollarSale;
	private String buyingEuro;
	private String saleOfEuro;
	private String purchaseOfTheRussianRuble;
	private String saleOfTheRussianRuble;

	public BasicInformation() {
		super();
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBuyingDollar() {
		return buyingDollar;
	}

	public void setBuyingDollar(String buyingDollar) {
		this.buyingDollar = buyingDollar;
	}

	public String getDollarSale() {
		return dollarSale;
	}

	public void setDollarSale(String dollarSale) {
		this.dollarSale = dollarSale;
	}

	public String getBuyingEuro() {
		return buyingEuro;
	}

	public void setBuyingEuro(String buyingEuro) {
		this.buyingEuro = buyingEuro;
	}

	public String getSaleOfEuro() {
		return saleOfEuro;
	}

	public void setSaleOfEuro(String saleOfEuro) {
		this.saleOfEuro = saleOfEuro;
	}

	public String getPurchaseOfTheRussianRuble() {
		return purchaseOfTheRussianRuble;
	}

	public void setPurchaseOfTheRussianRuble(String purchaseOfTheRussianRuble) {
		this.purchaseOfTheRussianRuble = purchaseOfTheRussianRuble;
	}

	public String getSaleOfTheRussianRuble() {
		return saleOfTheRussianRuble;
	}

	public void setSaleOfTheRussianRuble(String saleOfTheRussianRuble) {
		this.saleOfTheRussianRuble = saleOfTheRussianRuble;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bankName == null) ? 0 : bankName.hashCode());
		result = prime * result + ((buyingDollar == null) ? 0 : buyingDollar.hashCode());
		result = prime * result + ((buyingEuro == null) ? 0 : buyingEuro.hashCode());
		result = prime * result + ((dollarSale == null) ? 0 : dollarSale.hashCode());
		result = prime * result + ((purchaseOfTheRussianRuble == null) ? 0 : purchaseOfTheRussianRuble.hashCode());
		result = prime * result + ((saleOfEuro == null) ? 0 : saleOfEuro.hashCode());
		result = prime * result + ((saleOfTheRussianRuble == null) ? 0 : saleOfTheRussianRuble.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BasicInformation other = (BasicInformation) obj;
		if (bankName == null) {
			if (other.bankName != null)
				return false;
		} else if (!bankName.equals(other.bankName))
			return false;
		if (buyingDollar == null) {
			if (other.buyingDollar != null)
				return false;
		} else if (!buyingDollar.equals(other.buyingDollar))
			return false;
		if (buyingEuro == null) {
			if (other.buyingEuro != null)
				return false;
		} else if (!buyingEuro.equals(other.buyingEuro))
			return false;
		if (dollarSale == null) {
			if (other.dollarSale != null)
				return false;
		} else if (!dollarSale.equals(other.dollarSale))
			return false;
		if (purchaseOfTheRussianRuble == null) {
			if (other.purchaseOfTheRussianRuble != null)
				return false;
		} else if (!purchaseOfTheRussianRuble.equals(other.purchaseOfTheRussianRuble))
			return false;
		if (saleOfEuro == null) {
			if (other.saleOfEuro != null)
				return false;
		} else if (!saleOfEuro.equals(other.saleOfEuro))
			return false;
		if (saleOfTheRussianRuble == null) {
			if (other.saleOfTheRussianRuble != null)
				return false;
		} else if (!saleOfTheRussianRuble.equals(other.saleOfTheRussianRuble))
			return false;
		return true;
	}

	public BasicInformation(String bankName, String buyingDollar, String dollarSale, String buyingEuro,
			String saleOfEuro, String purchaseOfTheRussianRuble, String saleOfTheRussianRuble) {
		super();
		this.bankName = bankName;
		this.buyingDollar = buyingDollar;
		this.dollarSale = dollarSale;
		this.buyingEuro = buyingEuro;
		this.saleOfEuro = saleOfEuro;
		this.purchaseOfTheRussianRuble = purchaseOfTheRussianRuble;
		this.saleOfTheRussianRuble = saleOfTheRussianRuble;
	}

	@Override
	public String toString() {
		return "BasicInformation [bankName=" + bankName + ", buyingDollar=" + buyingDollar + ", dollarSale="
				+ dollarSale + ", buyingEuro=" + buyingEuro + ", saleOfEuro=" + saleOfEuro
				+ ", purchaseOfTheRussianRuble=" + purchaseOfTheRussianRuble + ", saleOfTheRussianRuble="
				+ saleOfTheRussianRuble + "]";
	}

}
